$(document).on('ready', function() {
  $('.fade').slick({
    dots: false,
    infinite: true,
    speed: 500,
    fade: true,
    cssEase: 'linear',
    autoplay: true,
    autoplaySpeed:5000,
    arrows:false,
    pauseOnFocus: false,
    pauseOnHover: false,
  });
});
$('#hide').hide().delay(2000).fadeIn(400);
// function showIt() {
//   document.getElementById("hide").style.visibility = "visible";
// }
// setTimeout("showIt()", 2000); // after 2 sec
